#!/bin/sh
KERNELSRC=/src/linux/bare
KERNELVRS=`gawk 'NR<=2{ v=v $3 "."} NR==3 {print v $3}' $KERNELSRC/Makefile`

HERE=`pwd`
#echo $HERE
#echo $KERNELVRS

rm -fR $KERNELVRS modules

mkdir -p $KERNELVRS
mkdir -p $KERNELVRS/modules
mkdir -p modules
(cd $KERNELSRC;make INSTALL_MOD_PATH=$HERE/modules modules_install)
find modules/lib -name "*.ko" -exec mv "{}" $HERE/$KERNELVRS/modules/ \;
git add $HERE/$KERNELVRS/modules/*
rm -fR modules

cp $KERNELSRC/.config $KERNELVRS/linux.config
cp $KERNELSRC/arch/i386/boot/bzImage $KERNELVRS/vmlinuz
git add $KERNELVRS/linux.config $KERNELVRS/vmlinuz

rm -f version
ln -sf $KERNELVRS version
