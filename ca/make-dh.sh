#!/bin/sh
OPENVPN_TOOLS=/usr/share/doc/openvpn/examples/easy-rsa/2.0
PKI=${OPENVPN_TOOLS}/pkitool

. ./vars
mkdir -p ./keys
${OPENVPN_TOOLS}/clean-all
${OPENVPN_TOOLS}/build-dh
