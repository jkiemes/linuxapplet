#!/bin/sh
OPENVPN_TOOLS=/usr/share/doc/openvpn/examples/easy-rsa/2.0
PKI=${OPENVPN_TOOLS}/pkitool

. ./vars
${PKI} --server $1
