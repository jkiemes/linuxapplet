APPLETS=${wildcard app_*}
ISO=${APPLETS:app_%=iso/%.iso}
DEPS=

all:	_all

-include .deps

_all:	.deps $(ISO)

.deps:	$(DEPS) Makefile
	rm -f .deps
	for i in $(APPLETS);do \
		echo -n "iso/$${i#app_}.iso:	ca/keys " >>.deps; \
		(find $$i -type f -name "*[^~]"; \
		 find $$i -type l -exec readlink -f "{}" \;) | grep -v "/host/" | xargs >>.deps; \
		echo  >>.deps; \
		echo  >>.deps; \
	done

iso/%.iso:
	mkdir -p iso
	mkdir -p root
	rm -fR $*_iso
	rm -fR root/$*
	(cd app_$*;rm -fR root;run-parts -v --exit-on-error mk.d)
	mv app_$*/root root/$*
	if [ -f root/$*/as_root.sh -a ! -f iso/installer.iso ]; then \
			make iso/installer.iso;\
	fi
	mkdir $*_iso
	cp -r iso_boot/. $*_iso
	(cd root/$*;find . | cpio --quiet -o -H newc >`echo ../../$*_iso/boot/osapplet.cpio`)
	if [ -f root/$*/as_root.sh ]; then 	\
		cat $*_iso/boot/osapplet.cpio $*_iso/boot/osapplet.cpio >tmp.img; \
		dd if=/dev/zero of=$*_iso/boot/osapplet.tar bs=1k count=100k; \
		qemu -cdrom iso/installer.iso -boot d \
			-drive file=tmp.img,media=disk,if=virtio  \
			-drive file=$*_iso/boot/osapplet.tar,media=disk,if=virtio; \
		tar -xf $*_iso/boot/osapplet.tar; \
		if [ -f osapplet ]; then \
			mv -f osapplet $*_iso/boot/osapplet.cpio; \
		fi; \
		rm $*_iso/boot/osapplet.tar; \
		rm tmp.img; \
	fi
	gzip -9 <$*_iso/boot/osapplet.cpio >$*_iso/boot/osapplet.gz
	rm $*_iso/boot/osapplet.cpio
	cp kernel/version/vmlinuz $*_iso/boot/
	mkisofs -l -J -R -V "Linux Applet $*" -no-emul-boot -boot-load-size 4 -boot-info-table \
		-b boot/isolinux/isolinux.bin -c boot/isolinux/boot.cat \
		-o iso/$*.iso $*_iso
	rm -fR $*_iso

disk/installer.hd:
	mkdir -p disk
	dd if=/dev/zero of=disk/installer.hd bs=1k count=500k
	mkfs.ext4 -F disk/installer.hd

test.%:	iso/%.iso app_%/host/run_app
	(cd app_$*/host;./run_app)

ca/keys:
	cd ca;make

proper:	clean
	rm -fR iso .deps

clean:
	find . -name "*~" -delete
	rm -fR */root root *_iso disk tmp.img
